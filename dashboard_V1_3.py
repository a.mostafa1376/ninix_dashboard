#in His Name
#ya Zahra

from bokeh.driving import count
from bokeh.models import CustomJS, Slider, Button, Range1d, ColumnDataSource, Span, Label
from bokeh.models.widgets import RangeSlider, Tabs, Panel, FileInput, DataTable, TableColumn, TextInput, RadioButtonGroup, DatePicker
from bokeh.plotting import curdoc, figure
from bokeh.palettes import Spectral4
from bokeh.layouts import column, row, gridplot, layout, WidgetBox, Spacer


import os
import numpy as np
import pandas as pd
import threading
import time
import datetime
import struct
import datetime
import mysql.connector


DB_SCHEMA = 'ninix_live_1.0.1'
DB_TEST_SCHEMA = 'ninix_staging_0.1.1'
DB_USERNAME = 'superhero'
DB_PASSWORD = 'wNSHy5v3vi27oaYlObXNK06L0tF/i+I9itBU'
DB_URL = 'localhost'
DB_PORT = '3306'
config = {
    'user': DB_USERNAME,
    'password': DB_PASSWORD,
    'host': DB_URL,
    'port': DB_PORT,
    'database': DB_SCHEMA,
}
#######################################################################################################################################
# Global Variables
Data = ''
BigData = []
data_index = 0

label_df = pd.DataFrame(columns=['start', 'end', 'label'])
label = ''
label_start_index = 0
label_end_index = 0


def update_dictionary(dictionary, packet):
    split = packet.split('-')
    
    dictionary['ax'] = int(split[1]+split[0],16)
    dictionary['ay'] = int(split[3]+split[2],16)
    dictionary['az'] = int(split[5]+split[4],16)
    
    x = bytes([int(split[6],16),int(split[7],16),int(split[8],16),int(split[9],16)])
    dictionary['f'] = struct.unpack('<f' , x)[0]
    if np.isnan(dictionary['f']):
        dictionary['f'] = 0
        print('invalid f')
    
    dictionary['nonf'] = int(split[11]+split[10],16)
    dictionary['ampngnr'] = int(split[13]+split[12],16)
    dictionary['th'] = int(split[14],16)
    
    payload = int(split[15],16)
    payload_flag = int(split[16],16)//16
    
    if payload_flag == 1:
        dictionary['temp'] = payload
    elif payload_flag == 2:
        dictionary['temp'] = (dictionary['temp']*256) + payload
        dictionary['temp'] = dictionary['temp']/1000
    elif payload_flag == 3:
        dictionary['c_temp'] = (payload+240)/10
    elif payload_flag == 4:
        dictionary['temp_validity'] = payload
    elif payload_flag == 5:
        dictionary['rr'] = payload
    elif payload_flag == 6:
        dictionary['rr_validity'] = payload
    elif payload_flag == 7:
        dictionary['hr'] = payload
    elif payload_flag == 8:
        dictionary['hr_validity'] = payload
    elif payload_flag == 9:
        dictionary['rh'] = payload



#    x = bin(int(split[16]))
#    dictionary['is_peak']       = int(x[-1])
#    dictionary['isTrue_peak']   = int(x[-2])
#    dictionary['isRet_peak']    = int(x[-3])
##########################################################    
    if dictionary['temp_validity'] == 0:
        dictionary['temp_validity'] = dictionary['temp']
    else:
        dictionary['temp_validity'] = -100
##########################################################
    
    return dictionary

parameters = ['ax', 'ay', 'az', 'f', 'nonf', 'ampngnr', 'th', 'temp', 'c_temp', 'rr', 'rr_validity', 'hr', 'rh', 'hr_validity', 'isRet_peak', 'isTrue_peak', 'is_peak', 'temp_validity']

dictionary = {key:0 for key in parameters}

source = ColumnDataSource({key:[] for key in parameters})

def updateBigData():
    global Data, data_index
    global BigData
    global dictionary
 
    while len(Data) > 0:
        data_index += 1
        idx = Data.find('\n')+1
        record = Data[:idx]
        Data = Data[idx:]
        i = record.find('(0x)')
        packet = record[i+5:i+64]
        if len(packet) != 59:
            print('invalid packet')
            return
        
        dictionary = update_dictionary(dictionary, packet)
        BigData.append(dictionary)


#######################################################################################################################################
ninix_id_textbox = TextInput(value="", title="Ninix ID:")

from_date = DatePicker(title = 'from')
from_time = TextInput(value="", title="HH:MM", width=60)

to_date = DatePicker(title = 'to')
to_time = TextInput(value="", title="HH:MM", width=60)

def getData():
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()

    ### query sample 1
    ninix_id = int(ninix_id_textbox.value)
    query_0 = (
        "SELECT * FROM nnx_kid"
        " WHERE device_uid = %s"
    )
    cursor.execute(query_0, (ninix_id,))
    for i in cursor:
        kid_id = i[0]
###########################################TO_DO###########################

###########################################################################

    query_1 = (
        "SELECT * FROM nnx_trackinglog"
        " WHERE (updated_at BETWEEN %s AND %s) AND (kid_id =  %s)"
        " ORDER BY updated_at ASC "
    )


#    start = datetime.datetime(2019, 1, 1, 11, 30)
#    end = datetime.datetime(2020, 1, 31, 11, 30)
    start = from_date.value
    end = to_date.value
    cursor.execute(query_1, (start, end, kid_id))


    for i in cursor:
        print((i))
        print('-' * 70)

    cursor.close()
    cnx.close()


button1 = Button(label='get Data', width=40, height=30, button_type="primary")
button1.on_click(getData)


p1 = figure(title='Filtered Respiration', plot_width=1000, plot_height=700)
p1.line("x", "f", color='blue', legend_label='f', alpha=0.8, source=source)
p1.circle("x", "f", color='blue', legend_label='f', alpha=0.8, source=source)

def label_selecting(attr, old, new):
    global data_index, label_start_index, label_end_index
    label_start_index = data_index - ROLLOVER + min(new)
    label_end_index = data_index - ROLLOVER + max(new)
source.selected.on_change('indices', label_selecting)

columns = [TableColumn(field="start", title="start"),
           TableColumn(field="end", title="end"),
           TableColumn(field='label', title='label')]
data_table = DataTable(source=ColumnDataSource(dict(label_df)), columns=columns, width=400, height=200)

label_text_input = TextInput(value="", title="Label:")

def label_button_onclick():
    global label_df, label_start_index, label_end_index, data_table
    df = pd.DataFrame({'start':[label_start_index], 'end':[label_end_index], 'label':[label_text_input.value]})
    label_df = label_df.append(df, ignore_index = True)
    data_table.source.data.update(dict(label_df))

    
label_button = Button(label='add label',width=40, height=45, button_type="primary")
label_button.on_click(label_button_onclick)

layout1 = layout([
    [WidgetBox(ninix_id_textbox)],
    [from_date, from_time, to_date, to_time],
    [WidgetBox(button1)],
    [p1],
    [Spacer(height=50)],
    row([column([label_text_input,label_button]), Spacer(width=100), data_table], background='beige')
])

doc = curdoc()
doc.add_root(layout1)
doc.title = "NINIX Dashboard"






