#in His Name
#ya Zahra

from bokeh.driving import count
from bokeh.models import CustomJS, Slider, Button, Range1d, ColumnDataSource, Span, Label
from bokeh.models.widgets import RangeSlider, Tabs, Panel, FileInput, DataTable, TableColumn, TextInput, Toggle
from bokeh.plotting import curdoc, figure
from bokeh.palettes import Spectral4
from bokeh.layouts import column, row, gridplot, layout, WidgetBox, Spacer


import os
import socket
import numpy as np
import pandas as pd
import threading
import time
import datetime
import struct


HOST = '192.168.0.121'
PORT = 12345

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = (HOST, PORT)
s.bind(server_address)

s.listen(1)

# Global Variables
Data = ''
data_index = 0
isRealTime = False

label_df = pd.DataFrame(columns=['start', 'end', 'label'])
label = ''
label_start_index = 0
label_end_index = 0
# 


def receive(socket):
    global Data, isRealTime
    s = socket

    while True:
        print('waiting for a connection')
        connection, address = s.accept()
        print(address)
        try:
            with open(str(address)+'   '+str(datetime.datetime.now())+'.txt', 'a') as file:
                while True:
                    data = connection.recv(1024)
                    if data:
                        data = data.decode('utf-8')
                        file.write(data)
                        file.flush()
                        if isRealTime:
                            Data += data
        except:
            pass
        finally:
            print('close')
            connection.close()
######################################################################################
###########################You can add functions here#################################
######################################################################################
length_of_packet = 80
miliseconds_per_frame = 17



def update_dictionary(dictionary, packet):
    split = packet.split('-')
    
    dictionary['ax'] = int(split[1]+split[0],16)
    dictionary['ay'] = int(split[3]+split[2],16)
    dictionary['az'] = int(split[5]+split[4],16)
    
    x = bytes([int(split[6],16),int(split[7],16),int(split[8],16),int(split[9],16)])
    dictionary['f'] = struct.unpack('<f' , x)[0]
    if np.isnan(dictionary['f']):
        dictionary['f'] = 0
        print('invalid f')
    
    dictionary['nonf'] = int(split[11]+split[10],16)
    dictionary['ampngnr'] = int(split[13]+split[12],16)
    dictionary['th'] = int(split[14],16)
    
    payload = int(split[15],16)
    payload_flag = int(split[16],16)//16
    
    if payload_flag == 1:
        dictionary['temp'] = payload
    elif payload_flag == 2:
        dictionary['temp'] = (dictionary['temp']*256) + payload
        dictionary['temp'] = dictionary['temp']/1000
    elif payload_flag == 3:
        dictionary['c_temp'] = (payload+240)/10
    elif payload_flag == 4:
        dictionary['temp_validity'] = payload
    elif payload_flag == 5:
        dictionary['rr'] = payload
    elif payload_flag == 6:
        dictionary['rr_validity'] = payload
    elif payload_flag == 7:
        dictionary['hr'] = payload
    elif payload_flag == 8:
        dictionary['hr_validity'] = payload
    elif payload_flag == 9:
        dictionary['rh'] = payload



#    x = bin(int(split[16]))
#    dictionary['is_peak']       = int(x[-1])
#    dictionary['isTrue_peak']   = int(x[-2])
#    dictionary['isRet_peak']    = int(x[-3])
##########################################################    
    if dictionary['temp_validity'] == 0:
        dictionary['temp_validity'] = dictionary['temp']
    else:
        dictionary['temp_validity'] = -100
##########################################################
    
    return dictionary

######################################################################################
######################################################################################
######################################################################################

UPDATE_INTERVAL = miliseconds_per_frame
ROLLOVER = 100

parameters = ['x', 'ax', 'ay', 'az', 'f', 'nonf', 'ampngnr', 'th', 'temp', 'c_temp', 'rr', 'rr_validity', 'hr', 'rh', 'hr_validity', 'isRet_peak', 'isTrue_peak', 'is_peak', 'temp_validity']

dictionary = {key:0 for key in parameters}

source = ColumnDataSource({key:[] for key in parameters})


@count()
def update(x):
    global Data, data_index
    global dictionary
    
    dictionary['x'] = x
 
    if len(Data) < length_of_packet:
        source.stream({key:[dictionary[key]] for key in dictionary.keys()}, rollover=ROLLOVER)
    else:
        data_index += 1
        idx = Data.find('\n')+1
        record = Data[:idx]
        Data = Data[idx:]
        i = record.find('(0x)')
        packet = record[i+5:i+64]
        if len(packet) != 59:
            print('invalid packet')
            return
        
        dictionary = update_dictionary(dictionary, packet)
        source.stream({key:[dictionary[key]] for key in dictionary.keys()}, rollover=ROLLOVER)


receive_thread = threading.Thread(target=receive, args=(s,))
receive_thread.start()

range_slider = RangeSlider(start=-1000, end=1000, value=(-100,100), step=10, title="y_index")


TOOLS = "box_select, box_zoom, lasso_select, reset, help"
p1 = figure(tools=TOOLS, title='Filtered Respiration', plot_width=500, plot_height=200, y_range=range_slider.value)
p1.line("x", "f", color='blue', legend_label='f', alpha=0.8, source=source)
p1.circle("x", "f", color='blue', legend_label='f', alpha=0.8, source=source)
#p1.background_fill_color = "smokywhite"


p2 = figure(tools=TOOLS, title='Respiration Rate', plot_width=500, plot_height=200, x_range=p1.x_range)
p2.line("x", "rr", color='green', legend_label='rr', alpha=0.8, source=source)
p2.circle("x", "rr", color='green', legend_label='rr', alpha=0.8, source=source)
#p2.background_fill_color = "smokywhite"

p3 = figure(tools=TOOLS, title='Temperature', plot_width=500, plot_height=200, x_range=p1.x_range, y_range=(25,40))
#p3.line("x", "p_temp", color='blue', legend_label='p_temp', alpha=0.8, source=source)
p3.line("x", "temp", color='green', legend_label='new_temp', alpha=0.8, source=source)
p3.scatter("x", "temp_validity", marker='circle_x', line_color="red", fill_color="red", fill_alpha=0.2, size=3, source=source)
#p3.background_fill_color = "smokywhite"
p3.legend.location = "top_right"
p3.legend.click_policy="hide"

p4 = figure(tools=TOOLS, title='Heart Rate', plot_width=500, plot_height=200, x_range=p1.x_range)
p4.line("x", "ampngnr", color='red', legend_label='ampngnr', alpha=0.8, source=source)
p4.circle("x", "ampngnr", color='red', legend_label='ampngnr', alpha=0.8, source=source)
#p4.background_fill_color = "smokywhite"

doc = curdoc()
#callback_id = doc.add_periodic_callback(update, UPDATE_INTERVAL)

file_input = FileInput(accept=".txt")

def play_pause():
    global callback_id, receive_thread
    global Data, isRealTime
    if button1.label == '► Play':
        button1.label = '❚❚ Pause'
        if len(file_input.filename) == 0 and (not receive_thread.is_alive()):
            receive_thread.start()
            callback_id = doc.add_periodic_callback(update, UPDATE_INTERVAL)
            return
        if len(file_input.filename) == 0 and receive_thread.is_alive():
            callback_id = doc.add_periodic_callback(update, UPDATE_INTERVAL)
            return

        if not isRealTime and len(Data) == 0:
            with open(file_input.filename, 'r') as data_file:
                data_text = data_file.readlines()
                Data = ''.join(data_text)
        callback_id = doc.add_periodic_callback(update, UPDATE_INTERVAL)
    else:
        button1.label = '► Play'
        curdoc().remove_periodic_callback(callback_id)

button1 = Button(label='► Play', width=40, height=30, button_type="primary")
button1.on_click(play_pause)


def back_to_real_time():
    global Data, isRealTime
    Data = ''
button2 = Button(label='back to Real Time', width=40, height=30, button_type="warning")
button2.on_click(back_to_real_time)
button2.visible = False

def realtime_toggle_onclick(arg):
    global Data, isRealTime
    isRealTime = not isRealTime
    button2.visible = isRealTime
    file_input.visible = not isRealTime
    if isRealTime:
        realtime_toggle.button_type = "success"
    else:
        realtime_toggle.button_type = "danger"
        
realtime_toggle = Toggle(label="RealTime", button_type="danger")
realtime_toggle.on_click(realtime_toggle_onclick)



def y_range_onchange(attr, old, new):
    global range_slider, p1
    p1.y_range.start = range_slider.value[0]
    p1.y_range.end = range_slider.value[1]
range_slider.on_change('value', y_range_onchange)
# def y_range_button_onclick():
#     global range_slider, p1
#     p1.y_range.start = range_slider.value[0]
#     p1.y_range.end = range_slider.value[1]
# y_range_button = Button(label='set y_range', width=40, height=45, button_type="success")
# y_range_button.on_click(y_range_button_onclick)






def label_selecting(attr, old, new):
    global data_index, label_start_index, label_end_index
    label_start_index = data_index - ROLLOVER + min(new)
    label_end_index = data_index - ROLLOVER + max(new)
source.selected.on_change('indices', label_selecting)

columns = [TableColumn(field="start", title="start"),
           TableColumn(field="end", title="end"),
           TableColumn(field='label', title='label')]
data_table = DataTable(source=ColumnDataSource(dict(label_df)), columns=columns, width=400, height=200)

label_text_input = TextInput(value="", title="Label:")

def label_button_onclick():
    global label_df, label_start_index, label_end_index, data_table
    df = pd.DataFrame({'start':[label_start_index], 'end':[label_end_index], 'label':[label_text_input.value]})
    label_df = label_df.append(df, ignore_index = True)
    data_table.source.data.update(dict(label_df))

    
label_button = Button(label='add label',width=40, height=45, button_type="primary")
label_button.on_click(label_button_onclick)




#label = Label(x=100, y=100, text='Real Time: '+str(isRealTime))
p = gridplot([[p1, p2], [p3, p4]])
#p.background = 'beige'

layout1 = layout([
    [WidgetBox(realtime_toggle)],
    [WidgetBox(file_input)],
    [WidgetBox(button1), Spacer(width=140), WidgetBox(button2)],
    [p],
    [WidgetBox(range_slider)],
    [Spacer(height=50)],
    row([column([label_text_input,label_button]), Spacer(width=100), data_table], background='beige')
])

######################################################################################
######################################Layout1#########################################
######################################################################################

# file_input = FileInput(accept=".txt")

# def real_time():
#     print('real')
# real_time_button = Button(label='Real Time', width=40, height=40)
# real_time_button.on_click(real_time)
# real_time = CustomJS(args=dict(tabs=tabs), code="""
#     tabs.active = (tabs.active + 1) % tabs.tabs.length
# """)
# l1 = layout([
#     file_input,
#     [WidgetBox(real_time_button)]
# ])

######################################################################################
######################################################################################
######################################################################################


#tab1 = Panel(child=l1,title="input")
#tab2 = Panel(child=l2,title="view")
#tabs = Tabs(tabs=[tab1, tab2])
doc.add_root(layout1)
doc.title = "NINIX Dashboard"

