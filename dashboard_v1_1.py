#in His Name
#ya Zahra

from bokeh.driving import count
from bokeh.models import CustomJS, Slider, Button, Range1d, ColumnDataSource, Span, Label
from bokeh.models.widgets import RangeSlider, Tabs, Panel, FileInput, DataTable, TableColumn, TextInput, RadioButtonGroup, DatePicker
from bokeh.plotting import curdoc, figure
from bokeh.palettes import Spectral4
from bokeh.layouts import column, row, gridplot, layout, WidgetBox, Spacer


import os
import numpy as np
import pandas as pd
import threading
import time
import datetime
import struct

# Global Variables
Data = ''
data_index = 0

label_df = pd.DataFrame(columns=['start', 'end', 'label'])
label = ''
label_start_index = 0
label_end_index = 0
# 
######################################################################################
###########################You can add functions here#################################
######################################################################################
length_of_packet = 80
miliseconds_per_frame = 17



def update_dictionary(dictionary, packet):
    split = packet.split('-')
    
    dictionary['ax'] = int(split[1]+split[0],16)
    dictionary['ay'] = int(split[3]+split[2],16)
    dictionary['az'] = int(split[5]+split[4],16)
    
    x = bytes([int(split[6],16),int(split[7],16),int(split[8],16),int(split[9],16)])
    dictionary['f'] = struct.unpack('<f' , x)[0]
    if np.isnan(dictionary['f']):
        dictionary['f'] = 0
        print('invalid f')
    
    dictionary['nonf'] = int(split[11]+split[10],16)
    dictionary['ampngnr'] = int(split[13]+split[12],16)
    dictionary['th'] = int(split[14],16)
    
    payload = int(split[15],16)
    payload_flag = int(split[16],16)//16
    
    if payload_flag == 1:
        dictionary['temp'] = payload
    elif payload_flag == 2:
        dictionary['temp'] = (dictionary['temp']*256) + payload
        dictionary['temp'] = dictionary['temp']/1000
    elif payload_flag == 3:
        dictionary['c_temp'] = (payload+240)/10
    elif payload_flag == 4:
        dictionary['temp_validity'] = payload
    elif payload_flag == 5:
        dictionary['rr'] = payload
    elif payload_flag == 6:
        dictionary['rr_validity'] = payload
    elif payload_flag == 7:
        dictionary['hr'] = payload
    elif payload_flag == 8:
        dictionary['hr_validity'] = payload
    elif payload_flag == 9:
        dictionary['rh'] = payload



#    x = bin(int(split[16]))
#    dictionary['is_peak']       = int(x[-1])
#    dictionary['isTrue_peak']   = int(x[-2])
#    dictionary['isRet_peak']    = int(x[-3])
##########################################################    
    if dictionary['temp_validity'] == 0:
        dictionary['temp_validity'] = dictionary['temp']
    else:
        dictionary['temp_validity'] = -100
##########################################################
    
    return dictionary

######################################################################################
######################################################################################
######################################################################################

UPDATE_INTERVAL = miliseconds_per_frame
ROLLOVER = 100

parameters = ['x', 'ax', 'ay', 'az', 'f', 'nonf', 'ampngnr', 'th', 'temp', 'c_temp', 'rr', 'rr_validity', 'hr', 'rh', 'hr_validity', 'isRet_peak', 'isTrue_peak', 'is_peak', 'temp_validity']

dictionary = {key:0 for key in parameters}

source = ColumnDataSource({key:[] for key in parameters})


@count()
def update(x):
    global Data, data_index
    global dictionary
    
    dictionary['x'] = x
 
    if len(Data) < length_of_packet:
        source.stream({key:[dictionary[key]] for key in dictionary.keys()}, rollover=ROLLOVER)
    else:
        data_index += 1
        idx = Data.find('\n')+1
        record = Data[:idx]
        Data = Data[idx:]
        i = record.find('(0x)')
        packet = record[i+5:i+64]
        if len(packet) != 59:
            print('invalid packet')
            return
        
        dictionary = update_dictionary(dictionary, packet)
        source.stream({key:[dictionary[key]] for key in dictionary.keys()}, rollover=ROLLOVER)


range_slider = RangeSlider(start=-1000, end=1000, value=(-100,100), step=10, title="y_index")


TOOLS = "box_select, box_zoom, lasso_select, reset, help"
p1 = figure(tools=TOOLS, title='Filtered Respiration', plot_width=500, plot_height=200, y_range=range_slider.value)
p1.line("x", "f", color='blue', legend_label='f', alpha=0.8, source=source)
p1.circle("x", "f", color='blue', legend_label='f', alpha=0.8, source=source)
#p1.background_fill_color = "smokywhite"


p2 = figure(tools=TOOLS, title='Respiration Rate', plot_width=500, plot_height=200, x_range=p1.x_range)
p2.line("x", "rr", color='green', legend_label='rr', alpha=0.8, source=source)
p2.circle("x", "rr", color='green', legend_label='rr', alpha=0.8, source=source)
#p2.background_fill_color = "smokywhite"

p3 = figure(tools=TOOLS, title='Temperature', plot_width=500, plot_height=200, x_range=p1.x_range, y_range=(25,40))
#p3.line("x", "p_temp", color='blue', legend_label='p_temp', alpha=0.8, source=source)
p3.line("x", "temp", color='green', legend_label='new_temp', alpha=0.8, source=source)
p3.scatter("x", "temp_validity", marker='circle_x', line_color="red", fill_color="red", fill_alpha=0.2, size=3, source=source)
#p3.background_fill_color = "smokywhite"
p3.legend.location = "top_right"
p3.legend.click_policy="hide"

p4 = figure(tools=TOOLS, title='Heart Rate', plot_width=500, plot_height=200, x_range=p1.x_range)
p4.line("x", "ampngnr", color='red', legend_label='ampngnr', alpha=0.8, source=source)
p4.circle("x", "ampngnr", color='red', legend_label='ampngnr', alpha=0.8, source=source)
#p4.background_fill_color = "smokywhite"

doc = curdoc()
#callback_id = doc.add_periodic_callback(update, UPDATE_INTERVAL)

file_input = TextInput(value="", title="Ninix ID:")

def play_pause():
    global callback_id, receive_thread
    global Data
    if button1.label == '► Play':
        button1.label = '❚❚ Pause'
        if len(Data) == 0:
            with open(file_input.value, 'r') as data_file:
                data_text = data_file.readlines()
                Data = ''.join(data_text)
        callback_id = doc.add_periodic_callback(update, UPDATE_INTERVAL)
    else:
        button1.label = '► Play'
        curdoc().remove_periodic_callback(callback_id)

button1 = Button(label='► Play', width=40, height=30, button_type="primary")
button1.on_click(play_pause)
###########################################################################################
from_date = DatePicker(title = 'from')
from_time = TextInput(value="", title="")

to_date = DatePicker(title = 'to')
to_time = TextInput(value="", title="")

last_n_record_slider = Slider(start=0, end=100, value=10, step=1, title="number of last records")
last_n_record_slider.visible = False


active_devices_from_date = DatePicker(title = 'Active Devices from')
active_devices_from_date.visible = False

visualize_mode_radio_button = RadioButtonGroup(labels=["Device Records by Date", "last Records", "Active Devices by Date"], active=0)

def visualize_mode_onchange(attr, old, new):
    global visualize_mode_radio_button
    if visualize_mode_radio_button.active == 0:
        from_date.visible = True
        to_date.visible = True
        file_input.visible = True
        last_n_record_slider.visible = False
        active_devices_from_date.visible = False
    elif visualize_mode_radio_button.active == 1:
        from_date.visible = False
        to_date.visible = False
        file_input.visible = True
        last_n_record_slider.visible = True
        active_devices_from_date.visible = False 
    elif visualize_mode_radio_button.active == 2:
        from_date.visible = False
        to_date.visible = False
        file_input.visible = False
        last_n_record_slider.visible = False
        active_devices_from_date.visible = True 
visualize_mode_radio_button.on_change('active', visualize_mode_onchange)


#############################################################################################
def y_range_onchange(attr, old, new):
    global range_slider, p1
    p1.y_range.start = range_slider.value[0]
    p1.y_range.end = range_slider.value[1]
range_slider.on_change('value', y_range_onchange)





def label_selecting(attr, old, new):
    global data_index, label_start_index, label_end_index
    label_start_index = data_index - ROLLOVER + min(new)
    label_end_index = data_index - ROLLOVER + max(new)
source.selected.on_change('indices', label_selecting)

columns = [TableColumn(field="start", title="start"),
           TableColumn(field="end", title="end"),
           TableColumn(field='label', title='label')]
data_table = DataTable(source=ColumnDataSource(dict(label_df)), columns=columns, width=400, height=200)

label_text_input = TextInput(value="", title="Label:")

def label_button_onclick():
    global label_df, label_start_index, label_end_index, data_table
    df = pd.DataFrame({'start':[label_start_index], 'end':[label_end_index], 'label':[label_text_input.value]})
    label_df = label_df.append(df, ignore_index = True)
    data_table.source.data.update(dict(label_df))

    
label_button = Button(label='add label',width=40, height=45, button_type="primary")
label_button.on_click(label_button_onclick)




#label = Label(x=100, y=100, text='Real Time: '+str(isRealTime))
p = gridplot([[p1, p2], [p3, p4]])
#p.background = 'beige'

layout1 = layout([
    [WidgetBox(visualize_mode_radio_button)],
    [WidgetBox(file_input)],
    [from_date, from_time, to_date, to_time, last_n_record_slider, active_devices_from_date],
    [WidgetBox(button1)],
    [p],
    [WidgetBox(range_slider)],
    [Spacer(height=50)],
    row([column([label_text_input,label_button]), Spacer(width=100), data_table], background='beige')
])

doc.add_root(layout1)
doc.title = "NINIX Dashboard"

