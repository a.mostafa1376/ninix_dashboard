#in His Name
#ya Zahra

from bokeh.driving import count
from bokeh.models import CustomJS, Slider, Button, Range1d, ColumnDataSource, Span, Label
from bokeh.models.widgets import RangeSlider, Tabs, Panel, FileInput, DataTable, TableColumn, TextInput, RadioButtonGroup, DatePicker, Select
from bokeh.plotting import curdoc, figure
from bokeh.palettes import Spectral4
from bokeh.layouts import column, row, gridplot, layout, WidgetBox, Spacer
from bokeh.models.annotations import Title
from bokeh.models import DatetimeTickFormatter

import os
import numpy as np
import pandas as pd
import threading
import time
import datetime
import struct
import math

import datetime
import mysql.connector
import json


DB_SCHEMA = 'ninix_live_1.0.1'
DB_TEST_SCHEMA = 'ninix_staging_0.1.1'
DB_USERNAME = 'superhero'
DB_PASSWORD = 'wNSHy5v3vi27oaYlObXNK06L0tF/i+I9itBU'
DB_URL = 'localhost'
DB_PORT = '3306'
config = {
    'user': DB_USERNAME,
    'password': DB_PASSWORD,
    'host': DB_URL,
    'port': DB_PORT,
    'database': DB_SCHEMA,
}


ninix_id_textbox = TextInput(value="", title="Ninix ID:")

from_date = DatePicker(title = 'from', value=datetime.date.today() ,max_date=datetime.date.today())
from_time = TextInput(value="", title="HH:MM", width=60)

to_date = DatePicker(title = 'to', value=datetime.date.today() ,max_date=datetime.date.today())
to_time = TextInput(value="", title="HH:MM", width=60)

last_records_textbox = TextInput(value="", title="number of last records:")

select_parameter = Select(title="parameter to plot:", options=[])


to_plot_Data = dict(index=[], y=[])

Data = []
ac_Data = []

source = ColumnDataSource(data=to_plot_Data)

TOOLS = "box_zoom, box_select, save, reset, help"
p = figure(tools=TOOLS, plot_width=1000, plot_height=300, x_axis_type="datetime")
p.line('index', 'y', color='blue', alpha=0.8, source=source)
p.circle('index', 'y', color='blue', alpha=0.8,  source=source)
p.xaxis.formatter = DatetimeTickFormatter(seconds = ['%H:%M:%S'], minsec = ['%H:%M:%S'], minutes = ['%H:%M:%S'], hourmin = ['%H:%M:%S'])
p.xaxis.major_label_orientation = math.pi/2




def get_nonf_list(c):
    split = [c[2*i:2*(i+1)] for i in range(20)]
    arr = np.array([])
    if int(split[0],16)%3 == 0:
        for i in range(1,13):
            x = int(split[i],16)
            if (x & 0x80) == 0x80:
            # if set, invert and add one to get the negative value, then add the negative sign
                x = -( (x ^ 0xff) + 1)
            arr = np.append(arr,x)
    else:
        for i in range(1,19):
            x = int(split[i],16)
            if (x & 0x80) == 0x80:
            # if set, invert and add one to get the negative value, then add the negative sign
                x = -( (x ^ 0xff) + 1)
            arr = np.append(arr,x)
        x = int(split[19][:2],16)
        if (x & 0x80) == 0x80:
        # if set, invert and add one to get the negative value, then add the negative sign
            x = -( (x ^ 0xff) + 1)
        arr = np.append(arr,x)
    return arr


def getData():
    global Data, ac_Data
    Data = []
    ac_Data = []
    
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()

    ### query sample 1
    ninix_id = int(ninix_id_textbox.value)
    query_0 = (
        "SELECT * FROM nnx_kid"
        " WHERE device_uid = %s"
    )
    cursor.execute(query_0, (ninix_id,))
    for i in cursor:
        kid_id = i[0]


    query_1 = (
        "SELECT * FROM nnx_trackinglog"
        " ORDER BY updated_at DESC LIMIT %s"
        
    )
    n_last_records = int(last_records_textbox.value)
    cursor.execute(query_1, (n_last_records,))


    for i in cursor:
        packet = json.loads(i[-3])
        if 'rR' in packet.keys():
            Data.append(packet)
    if len(Data) > 0:
        Data = pd.DataFrame(Data)
        Data['tS'] = Data['tS'].astype(float)
        Data = Data.sort_values(by=['tS'])
        select_parameter.options = list(Data.columns)
        Data['time'] = Data['tS'].map(lambda x : datetime.datetime.fromtimestamp(x//1000) + datetime.timedelta(hours=3, minutes=30))
        Data['time'] = pd.to_datetime(Data['time'])
        
        to_plot_Data['index'] = Data['time']
        to_plot_Data['y'] = Data['rR']

        
        
        for s in Data['ac']:
            if len(s) == 120:
                ac_Data.append(s[0:40])
                ac_Data.append(s[40:80])
                ac_Data.append(s[80:120])
        ac_Data = pd.DataFrame(ac_Data)
        ac_Data['ls'] = ac_Data[0].map(get_nonf_list)
        ac_Data = np.concatenate(ac_Data['ls'])
        
    source.data = to_plot_Data

    cursor. close()
    cnx.close()


button1 = Button(label='get Data', width=40, height=30, button_type="primary")
button1.on_click(getData)

def plot_parameter(attr, old, new):
    parameter = select_parameter.value
    if len(Data) > 0:
        if parameter == 'ac':
            to_plot_Data['y'] = ac_Data
            to_plot_Data['index'] = range(len(ac_Data))
        else:
            to_plot_Data['y'] = Data[parameter]
            to_plot_Data['index'] = Data['time']
        p.title.text = parameter
        source.data.update(to_plot_Data)
        

select_parameter.on_change('value', plot_parameter)


layout1 = layout([
    [WidgetBox(ninix_id_textbox)],
#    [from_date, from_time, to_date, to_time],
    [last_records_textbox],
    [WidgetBox(button1)],
    [select_parameter],
    [p]
#    [Spacer(height=50)],
#    row([column([label_text_input,label_button]), Spacer(width=100), data_table], background='beige')
])

doc = curdoc()
doc.add_root(layout1)
doc.title = "NINIX Dashboard"






