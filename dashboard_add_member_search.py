#in His Name
#ya Zahra

from bokeh.driving import count
from bokeh.models import CustomJS, Slider, Button, Range1d, ColumnDataSource, Span, Label
from bokeh.models.widgets import RangeSlider, Tabs, Panel, FileInput, DataTable, TableColumn, TextInput, RadioButtonGroup, DatePicker, Select
from bokeh.plotting import curdoc, figure
from bokeh.palettes import Spectral4
from bokeh.layouts import column, row, gridplot, layout, WidgetBox, Spacer
from bokeh.models.annotations import Title
from bokeh.models import DatetimeTickFormatter, DateFormatter


import os
from os.path import dirname, join
import numpy as np
import pandas as pd
import threading
import time
import datetime
import struct
import math

import datetime
import mysql.connector
import json


DB_SCHEMA = 'ninix_live_1.0.1'
DB_TEST_SCHEMA = 'ninix_staging_0.1.1'
DB_USERNAME = 'superhero'
DB_PASSWORD = 'wNSHy5v3vi27oaYlObXNK06L0tF/i+I9itBU'
DB_URL = 'localhost'
DB_PORT = '3306'
config = {
    'user': DB_USERNAME,
    'password': DB_PASSWORD,
    'host': DB_URL,
    'port': DB_PORT,
    'database': DB_SCHEMA,
}

now = datetime.datetime.now() + datetime.timedelta(hours=3, minutes=30)

visualize_mode_radio_button = RadioButtonGroup(labels=["get Active Devices", "Device Records (nnx_trackinglog)", "Device Records (nnx_rawlog)"], active=0)
ninix_id_textbox = TextInput(value="", title="Ninix ID:")

from_date = DatePicker(title = 'from', value=datetime.date.today() ,max_date=datetime.date.today())
from_time = TextInput(value=str(now.hour)+":"+str(now.minute), title="HH:MM", width=60)

to_date = DatePicker(title = 'to', value=datetime.date.today() ,max_date=datetime.date.today())
to_time = TextInput(value=str(now.hour)+":"+str(now.minute), title="HH:MM", width=60)

last_records_textbox = TextInput(value="", title="number of last records:")
select_parameter1 = Select(width=140, title="parameter1 to plot:", options=[])
parameter1_scale = TextInput(width=50, value="1", title="Scale:")
parameter1_bias = TextInput(width=50, value="0", title="Bias:")

select_parameter2 = Select(width=140, title="parameter2 to plot:", options=[])
parameter2_scale = TextInput(width=50, value="1", title="Scale:")
parameter2_bias = TextInput(width=50, value="0", title="Bias:")

select_parameter3 = Select(width=140, title="parameter3 to plot:", options=[])
parameter3_scale = TextInput(width=50, value="1", title="Scale:")
parameter3_bias = TextInput(width=50, value="0", title="Bias:")





to_plot_Data = dict(index=[], y=[])

Data = []
ac_Data = []

table_df = pd.DataFrame(columns=['device_id', 'first_name', 'last_name'])
columns = [TableColumn(field="device_id", title="device_id"),
           TableColumn(field="first_name", title="first_name"),
           TableColumn(field='last_name', title='last_name')]
data_table = DataTable(source=ColumnDataSource(dict(table_df)), columns=columns, width=400, height=500)

columns_from_rawlog = ['number', 'my_message', 'phone', 'versions', 'log_message','ts', 'time']
temp_Data = pd.DataFrame(columns=columns_from_rawlog)
columns_from_rawlog = [TableColumn(field=Ci, title=Ci) for Ci in columns_from_rawlog[:-1]]
columns_from_rawlog.append(TableColumn(field='time', title='time', width=400, formatter=DateFormatter(format="%d/%m/%Y %H:%M:%S")))

data_table_from_rawlog = DataTable(source=ColumnDataSource(dict(temp_Data)), columns=columns_from_rawlog, width=760, height=200)


source_to_download = ColumnDataSource(data=dict())

source1 = ColumnDataSource(data=to_plot_Data)
source2 = ColumnDataSource(data=to_plot_Data)
source3 = ColumnDataSource(data=to_plot_Data)

TOOLS = "box_select, box_zoom, save, reset, help"
p = figure(tools=TOOLS, plot_width=760, plot_height=300, x_axis_type="datetime")

p.line('index', 'y', color='blue', alpha=0.8, source=source1, legend_label='parameter1')
p.circle('index', 'y', color='blue', alpha=0.8,  source=source1, legend_label='parameter1')

p.line('index', 'y', color='red', alpha=0.8, source=source2, legend_label='parameter2')
p.circle('index', 'y', color='red', alpha=0.8,  source=source2, legend_label='parameter2')

p.line('index', 'y', color='green', alpha=0.8, source=source3, legend_label='parameter3')
p.circle('index', 'y', color='green', alpha=0.8,  source=source3, legend_label='parameter3')
p.legend.location = "top_left"
p.legend.click_policy="hide"
p.xaxis.formatter = DatetimeTickFormatter(seconds = ['%H:%M:%S'], minsec = ['%H:%M:%S'], minutes = ['%H:%M:%S'], hourmin = ['%H:%M:%S'])
p.xaxis.major_label_orientation = math.pi/2




where_data = dict(index=[], y=[])
source_where_data = ColumnDataSource(data=where_data)

TOOLS = "box_select, box_zoom, reset, save, help"
p0 = figure(tools=TOOLS, plot_width=760, plot_height=200, x_axis_type="datetime")
p0.circle('index', 'y', color='green', alpha=0.8,  source=source_where_data)
p0.xaxis.formatter = DatetimeTickFormatter(seconds = ["%m/%d %H:%M"], minsec = ["%m/%d %H:%M"], hourmin = ["%m/%d %H:%M"],  days=["%m/%d %H:%M"],months=["%m/%d %H:%M"],hours=["%m/%d %H:%M"],minutes=["%m/%d %H:%M"])
p0.xaxis.major_label_orientation = math.pi/4
def whereisData():
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()
    
    if visualize_mode_radio_button.active == 1:
        ninix_id = int(ninix_id_textbox.value)
        query_0 = (
            "SELECT * FROM nnx_kid"
            " WHERE device_uid = %s"
        )
        cursor.execute(query_0, (ninix_id,))
        for i in cursor:
            kid_id = i[0]


        query_1 = (
            "SELECT * FROM nnx_trackinglog"
            " WHERE (kid_id =  %s)"
            " ORDER BY updated_at ASC "
        )

        cursor.execute(query_1, (kid_id,))

        temp_Data = []
        for i in cursor:
            packet = json.loads(i[-3])
            if 'rR' in packet.keys():
                temp_Data.append(packet)
        if len(temp_Data) > 0:
            temp_Data = pd.DataFrame(temp_Data)
            temp_Data['tS'] = temp_Data['tS'].astype(float)
            temp_Data = temp_Data.sort_values(by=['tS'])
            temp_Data['time'] = temp_Data['tS'].map(lambda x : datetime.datetime.fromtimestamp(x//1000) + datetime.timedelta(hours=3, minutes=30))
            temp_Data['time'] = pd.to_datetime(temp_Data['time'])
            temp_Data['1'] = 1
            where_data['index'] = temp_Data['time']
            where_data['y'] = temp_Data['1']
            p0.x_range.start = min(where_data['index'])
            p0.x_range.end = max(where_data['index'])
            p0.y_range.start = 0.5
            p0.y_range.end = 1.5
            source_where_data.data.update(where_data)
            
    elif visualize_mode_radio_button.active == 2:
        ninix_id = int(ninix_id_textbox.value)
        query_0 = (
            "SELECT * FROM nnx_rawlog"
            " WHERE device_uid = %s"
        )
        
        cursor.execute(query_0, (ninix_id,))
        temp_Data = []
        for i in cursor:
            packet = json.loads(i[-2])
            temp_Data.append(packet)
        if len(temp_Data) > 0:
            temp_Data = pd.DataFrame(temp_Data)
            temp_Data['ts'] = temp_Data['ts'].astype(float)
            temp_Data = temp_Data.sort_values(by=['ts'])
            temp_Data['time'] = temp_Data['ts'].map(lambda x : datetime.datetime.fromtimestamp(x//1000) + datetime.timedelta(hours=3, minutes=30))
            temp_Data['time'] = pd.to_datetime(temp_Data['time'])
            temp_Data['1'] = 1
            where_data['index'] = temp_Data['time']
            where_data['y'] = temp_Data['1']
            p0.x_range.start = min(where_data['index'])
            p0.x_range.end = max(where_data['index'])
            p0.y_range.start = 0.5
            p0.y_range.end = 1.5
            source_where_data.data.update(where_data)

    cursor.close()
    cnx.close()
    
whereisDatabutton = Button(label='where is Data', width=40, height=30, button_type="primary")
whereisDatabutton.on_click(whereisData)

def datetime_selecting(attr, old, new):
    from_datetime = where_data['index'][min(new)]
    to_datetime = where_data['index'][max(new)]
    
    from_date.value = from_datetime.date()
    from_time.value = str(from_datetime.hour) + ':' + str(from_datetime.minute)
    
    to_date.value = to_datetime.date()
    to_time.value = str(to_datetime.hour) + ':' + str(to_datetime.minute)
    
source_where_data.selected.on_change('indices', datetime_selecting)



def get_nonf_list(c):
    split = [c[2*i:2*(i+1)] for i in range(20)]
    arr = np.array([])
    if int(split[0],16)%3 == 0:
        for i in range(1,13):
            x = int(split[i],16)
            if (x & 0x80) == 0x80:
            # if set, invert and add one to get the negative value, then add the negative sign
                x = -( (x ^ 0xff) + 1)
            arr = np.append(arr,x)
    else:
        for i in range(1,19):
            x = int(split[i],16)
            if (x & 0x80) == 0x80:
            # if set, invert and add one to get the negative value, then add the negative sign
                x = -( (x ^ 0xff) + 1)
            arr = np.append(arr,x)
        x = int(split[19][:2],16)
        if (x & 0x80) == 0x80:
        # if set, invert and add one to get the negative value, then add the negative sign
            x = -( (x ^ 0xff) + 1)
        arr = np.append(arr,x)
    return arr


def get_ax_dc(c):
    c = c[80:120]
    split = [c[2*i:2*(i+1)] for i in range(20)]
    x = int(split[14]+split[13],16)
    if (x & 0x8000) == 0x8000:
    # if set, invert and add one to get the negative value, then add the negative sign
        x = -( (x ^ 0xffff) + 1)
    return x
def get_ay_dc(c):
    c = c[80:120]
    split = [c[2*i:2*(i+1)] for i in range(20)]
    x = int(split[16]+split[15],16)
    if (x & 0x8000) == 0x8000:
    # if set, invert and add one to get the negative value, then add the negative sign
        x = -( (x ^ 0xffff) + 1)
    return x
def get_az_dc(c):
    c = c[80:120]
    split = [c[2*i:2*(i+1)] for i in range(20)]
    x = int(split[18]+split[17],16)
    if (x & 0x8000) == 0x8000:
    # if set, invert and add one to get the negative value, then add the negative sign
        x = -( (x ^ 0xffff) + 1)
    return x


def getData():
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()
    if visualize_mode_radio_button.active == 1:
        global Data, ac_Data
        Data = []
        ac_Data = []

        ninix_id = int(ninix_id_textbox.value)
        query_0 = (
            "SELECT * FROM nnx_kid"
            " WHERE device_uid = %s"
        )
        cursor.execute(query_0, (ninix_id,))
        for i in cursor:
            kid_id = i[0]

        
        query_1 = (
            "SELECT * FROM nnx_trackinglog"
            " WHERE (updated_at BETWEEN %s AND %s) AND (kid_id =  %s)"
            " ORDER BY updated_at ASC "
        )

        d = from_date.value
        t = from_time.value.split(':')
        start = datetime.datetime(d.year, d.month, d.day, int(t[0]), int(t[1]))
        start = start - datetime.timedelta(hours=3, minutes=30)


        d = to_date.value
        t = to_time.value.split(':')
        end = datetime.datetime(d.year, d.month, d.day, int(t[0]), int(t[1]))
        end = end - datetime.timedelta(hours=3, minutes=30)

        cursor.execute(query_1, (start, end, kid_id))       
        for i in cursor:
            packet = json.loads(i[-3])
            if 'rR' in packet.keys():
                Data.append(packet)
        if len(Data) > 0:
            Data = pd.DataFrame(Data)
            Data['tS'] = Data['tS'].astype(float)
            Data = Data.sort_values(by=['tS'])
            select_parameter1.options = list(np.concatenate((['None'], Data.columns, ['ax_dc', 'ay_dc', 'az_dc'])))
            select_parameter2.options = list(np.concatenate((['None'], Data.columns, ['ax_dc', 'ay_dc', 'az_dc'])))
            select_parameter3.options = list(np.concatenate((['None'], Data.columns, ['ax_dc', 'ay_dc', 'az_dc'])))
            Data['time'] = Data['tS'].map(lambda x : datetime.datetime.fromtimestamp(x//1000) + datetime.timedelta(hours=3, minutes=30))
            Data['time'] = pd.to_datetime(Data['time'])

            Data['ax_dc'] = Data['ac'].map(get_ax_dc)
            Data['ay_dc'] = Data['ac'].map(get_ay_dc)
            Data['az_dc'] = Data['ac'].map(get_az_dc)

            for s in Data['ac']:
                if len(s) == 120:
                    ac_Data.append(s[0:40])
                    ac_Data.append(s[40:80])
                    ac_Data.append(s[80:120])
            ac_Data = pd.DataFrame(ac_Data)
            ac_Data['ls'] = ac_Data[0].map(get_nonf_list)
            ac_Data = np.concatenate(ac_Data['ls'])

            source_to_download.data = {key:Data[key] for key in Data.columns}

        source1.data = to_plot_Data
        source2.data = dict(index=[], y=[])
        source3.data = dict(index=[], y=[])
    
    elif visualize_mode_radio_button.active == 2:
        ninix_id = int(ninix_id_textbox.value)
        query_0 = (
            "SELECT * FROM nnx_rawlog"
            " WHERE device_uid = %s"
        )
        
        cursor.execute(query_0, (ninix_id,))
        temp_Data = []
        for i in cursor:
            packet = json.loads(i[-2])
            temp_Data.append({'number':packet['number'], 'my_message':packet['my_message'], 'phone':packet['phone'], 'versions':packet['versions'], 'log_message':i[-4], 'ts':packet['ts']})
        if len(temp_Data) > 0:
            temp_Data = pd.DataFrame(temp_Data)
            temp_Data['ts'] = temp_Data['ts'].astype(float)
            temp_Data = temp_Data.sort_values(by=['ts'])
            temp_Data['time'] = temp_Data['ts'].map(lambda x : datetime.datetime.fromtimestamp(x//1000) + datetime.timedelta(hours=3, minutes=30))
            temp_Data['time'] = pd.to_datetime(temp_Data['time'])
            
            source_to_download.data = {key:temp_Data[key] for key in temp_Data.columns}
            data_table_from_rawlog.source.data.update(dict(temp_Data))
    cursor.close()
    cnx.close()


button1 = Button(label='get Data', width=40, height=30, button_type="primary")
button1.on_click(getData)

    
button2 = Button(label='Download Data as csv', width=40, height=30, button_type="primary")
button2.js_on_click(CustomJS(args=dict(source=source_to_download),code=open(join(dirname(__file__), "download.js")).read()))
    

def getActiveDevices_tracking():
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()
    
    query_0 = (
        "SELECT DISTINCT kid_id FROM nnx_trackinglog"
        " WHERE created_at BETWEEN %s AND %s"
    )
    
    d = from_date.value
    t = from_time.value.split(':')
    start = datetime.datetime(d.year, d.month, d.day, int(t[0]), int(t[1]))
    start = start - datetime.timedelta(hours=3, minutes=30)


    d = to_date.value
    t = to_time.value.split(':')
    end = datetime.datetime(d.year, d.month, d.day, int(t[0]), int(t[1]))
    end = end - datetime.timedelta(hours=3, minutes=30)

    cursor.execute(query_0, (start, end))

    kid_ids = []

    for i in cursor:
        kid_ids.append(int(i[0]))
        
    query_1 = (
        "SELECT * FROM nnx_kid WHERE id IN (" + (','.join([str(kid) for kid in kid_ids])) + ")"
    )

    table_df = []
    cursor.execute(query_1)
    for i in cursor:
        table_df.append({'device_id':i[7], 'first_name':i[3],'last_name':i[4]})

    table_df = pd.DataFrame(table_df)
    data_table.source.data.update(dict(table_df))
    
    cursor.close()
    cnx.close()

button3 = Button(label='get Active Devices (nnx_trackinglog)', height=30, button_type="primary")
button3.on_click(getActiveDevices_tracking)


def getActiveDevices_raw():
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()
    
    query_0 = (
        "SELECT DISTINCT device_uid FROM nnx_rawlog"
        " WHERE created_at BETWEEN %s AND %s"
    )

    
    d = from_date.value
    t = from_time.value.split(':')
    start = datetime.datetime(d.year, d.month, d.day, int(t[0]), int(t[1]))
    start = start - datetime.timedelta(hours=3, minutes=30)


    d = to_date.value
    t = to_time.value.split(':')
    end = datetime.datetime(d.year, d.month, d.day, int(t[0]), int(t[1]))
    end = end - datetime.timedelta(hours=3, minutes=30)

    cursor.execute(query_0, (start, end))

    device_ids = []

    for i in cursor:
        device_ids.append(int(i[0]))
    
    query_1 = (
        "SELECT * FROM nnx_kid WHERE device_uid IN (" + (','.join([str(dev_id) for dev_id in device_ids])) + ")"
    )

    table_df = []
    cursor.execute(query_1)
    for i in cursor:
        table_df.append({'device_id':i[7], 'first_name':i[3],'last_name':i[4]})

    table_df = pd.DataFrame(table_df)
    data_table.source.data.update(dict(table_df))
    cursor.close()
    cnx.close()

button4 = Button(label='get Active Devices (nnx_rawlog)', height=30, button_type="primary")
button4.on_click(getActiveDevices_raw)



def callback(attrname, old, new):
    selected_index = data_table.source.selected.indices[0]
    ninix_id_textbox.value = str(data_table.source.data["device_id"][selected_index])
data_table.source.selected.on_change('indices', callback)




def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False

def plot_parameter1(attr, old, new):
    parameter = select_parameter1.value
    if len(Data) > 0:
        to_plot_Data = dict(index=[], y=[])
        if parameter == 'None':
            to_plot_Data = dict(index=[], y=[])
        elif parameter == 'ac':
            to_plot_Data['y'] = ac_Data
            to_plot_Data['index'] = range(len(ac_Data))
            if isfloat(parameter1_scale.value) and isfloat(parameter1_bias.value):
                to_plot_Data['y'] = (to_plot_Data['y']*float(parameter1_scale.value)) + float(parameter1_bias.value)
        else:
            to_plot_Data['y'] = Data[parameter]
            to_plot_Data['index'] = Data['time']
            if isfloat(parameter1_scale.value) and isfloat(parameter1_bias.value):
                to_plot_Data['y'] = (to_plot_Data['y']*float(parameter1_scale.value)) + float(parameter1_bias.value)
        source1.data.update(to_plot_Data)
select_parameter1.on_change('value', plot_parameter1)
# parameter1_scale.on_change('value', plot_parameter1)
# parameter1_bias.on_change('value', plot_parameter1)

def plot_parameter2(attr, old, new):
    parameter = select_parameter2.value
    if len(Data) > 0:
        to_plot_Data = dict(index=[], y=[])
        if parameter == 'None':
            to_plot_Data = dict(index=[], y=[])
        elif parameter == 'ac':
            to_plot_Data['y'] = ac_Data
            to_plot_Data['index'] = range(len(ac_Data))
            if isfloat(parameter2_scale.value) and isfloat(parameter2_bias.value):
                to_plot_Data['y'] = (to_plot_Data['y']*float(parameter2_scale.value)) + float(parameter2_bias.value)
        else:
            to_plot_Data['y'] = Data[parameter]
            to_plot_Data['index'] = Data['time']
            if isfloat(parameter2_scale.value) and isfloat(parameter2_bias.value):
                to_plot_Data['y'] = (to_plot_Data['y']*float(parameter2_scale.value)) + float(parameter2_bias.value)
        source2.data.update(to_plot_Data)
select_parameter2.on_change('value', plot_parameter2)
# parameter2_scale.on_change('value', plot_parameter2)
# parameter2_bias.on_change('value', plot_parameter2)

def plot_parameter3(attr, old, new):
    parameter = select_parameter3.value
    if len(Data) > 0:
        to_plot_Data = dict(index=[], y=[])
        if parameter == 'None':
            to_plot_Data = dict(index=[], y=[])
        elif parameter == 'ac':
            to_plot_Data['y'] = ac_Data
            to_plot_Data['index'] = range(len(ac_Data))
            if isfloat(parameter3_scale.value) and isfloat(parameter3_bias.value):
                to_plot_Data['y'] = (to_plot_Data['y']*float(parameter3_scale.value)) + float(parameter3_bias.value)
        else:
            to_plot_Data['y'] = Data[parameter]
            to_plot_Data['index'] = Data['time']
            if isfloat(parameter3_scale.value) and isfloat(parameter3_bias.value):
                to_plot_Data['y'] = (to_plot_Data['y']*float(parameter3_scale.value)) + float(parameter3_bias.value)
        source3.data.update(to_plot_Data)
select_parameter3.on_change('value', plot_parameter3)
# parameter3_scale.on_change('value', plot_parameter3)
# parameter3_bias.on_change('value', plot_parameter3)

def refreshPlot1():
    global Data
    parameter = select_parameter1.value
    if len(Data) > 0:
        to_plot_Data = dict(index=[], y=[])
        if parameter == 'None':
            to_plot_Data = dict(index=[], y=[])
        elif parameter == 'ac':
            to_plot_Data['y'] = ac_Data
            to_plot_Data['index'] = range(len(ac_Data))
            if isfloat(parameter1_scale.value) and isfloat(parameter1_bias.value):
                to_plot_Data['y'] = (to_plot_Data['y']*float(parameter1_scale.value)) + float(parameter1_bias.value)
        else:
            to_plot_Data['y'] = Data[parameter]
            to_plot_Data['index'] = Data['time']
            if isfloat(parameter1_scale.value) and isfloat(parameter1_bias.value):
                to_plot_Data['y'] = (to_plot_Data['y']*float(parameter1_scale.value)) + float(parameter1_bias.value)
        source1.data.update(to_plot_Data)
    
       
        
refreshPlotButton1 = Button(label='Plot', width=50, height=30, button_type="primary")
refreshPlotButton1.on_click(refreshPlot1)

def refreshPlot2():
    global Data
    parameter = select_parameter2.value
    if len(Data) > 0:
        to_plot_Data = dict(index=[], y=[])
        if parameter == 'None':
            to_plot_Data = dict(index=[], y=[])
        elif parameter == 'ac':
            to_plot_Data['y'] = ac_Data
            to_plot_Data['index'] = range(len(ac_Data))
            if isfloat(parameter2_scale.value) and isfloat(parameter2_bias.value):
                to_plot_Data['y'] = (to_plot_Data['y']*float(parameter2_scale.value)) + float(parameter2_bias.value)
        else:
            to_plot_Data['y'] = Data[parameter]
            to_plot_Data['index'] = Data['time']
            if isfloat(parameter2_scale.value) and isfloat(parameter2_bias.value):
                to_plot_Data['y'] = (to_plot_Data['y']*float(parameter2_scale.value)) + float(parameter2_bias.value)
        source2.data.update(to_plot_Data)    
    
        
refreshPlotButton2 = Button(label='Plot', width=50, height=30, button_type="primary")
refreshPlotButton2.on_click(refreshPlot2)        


def refreshPlot3():
    global Data
    parameter = select_parameter3.value
    if len(Data) > 0:
        to_plot_Data = dict(index=[], y=[])
        if parameter == 'None':
            to_plot_Data = dict(index=[], y=[])
        elif parameter == 'ac':
            to_plot_Data['y'] = ac_Data
            to_plot_Data['index'] = range(len(ac_Data))
            if isfloat(parameter3_scale.value) and isfloat(parameter3_bias.value):
                to_plot_Data['y'] = (to_plot_Data['y']*float(parameter3_scale.value)) + float(parameter3_bias.value)
        else:
            to_plot_Data['y'] = Data[parameter]
            to_plot_Data['index'] = Data['time']
            if isfloat(parameter3_scale.value) and isfloat(parameter3_bias.value):
                to_plot_Data['y'] = (to_plot_Data['y']*float(parameter3_scale.value)) + float(parameter3_bias.value)
        source3.data.update(to_plot_Data)
        
refreshPlotButton3 = Button(label='Plot', width=50, height=30, button_type="primary")
refreshPlotButton3.on_click(refreshPlot3) 



#"Active Devices (nnx_trackinglog)", "Active Devices (nnx_rawlog)", "Device Records by Date"
def visualize_mode_onchange(attr, old, new):
    if visualize_mode_radio_button.active == 0:
        ninix_id_textbox.visible = False
        from_date.visible = True
        from_time.visible = True
        to_date.visible = True
        to_time.visible = True
        last_records_textbox.visible = False
        button1.visible = False
        
        select_parameter1.visible = False
        select_parameter2.visible = False
        select_parameter3.visible = False
        parameter1_bias.visible = False
        parameter2_bias.visible = False
        parameter3_bias.visible = False
        parameter1_scale.visible = False
        parameter2_scale.visible = False
        parameter3_scale.visible = False
        refreshPlotButton1.visible = False
        refreshPlotButton2.visible = False
        refreshPlotButton3.visible = False
        
        p.visible = False
        button2.visible = False
        button3.visible = True
        button4.visible = True
        whereisDatabutton.visible = False
        p0.visible = False
        data_table_from_rawlog.visible = False
        
    elif visualize_mode_radio_button.active == 1:
        ninix_id_textbox.visible = True
        from_date.visible = True
        from_time.visible = True
        to_date.visible = True
        to_time.visible = True
        last_records_textbox.visible = False
        button1.visible = True
        select_parameter1.visible = True
        select_parameter2.visible = True
        select_parameter3.visible = True
        parameter1_bias.visible = True
        parameter2_bias.visible = True
        parameter3_bias.visible = True
        parameter1_scale.visible = True
        parameter2_scale.visible = True
        parameter3_scale.visible = True
        refreshPlotButton1.visible = True
        refreshPlotButton2.visible = True
        refreshPlotButton3.visible = True
        
        p.visible = True
        button2.visible = True
        button3.visible = False
        button4.visible = False
        whereisDatabutton.visible = True
        p0.visible = True
        data_table_from_rawlog.visible = False
        
    elif visualize_mode_radio_button.active == 2:
        ninix_id_textbox.visible = True
        from_date.visible = False
        from_time.visible = False
        to_date.visible = False
        to_time.visible = False
        last_records_textbox.visible = False
        button1.visible = True
        select_parameter1.visible = False
        select_parameter2.visible = False
        select_parameter3.visible = False
        parameter1_bias.visible = False
        parameter2_bias.visible = False
        parameter3_bias.visible = False
        parameter1_scale.visible = False
        parameter2_scale.visible = False
        parameter3_scale.visible = False
        refreshPlotButton1.visible = False
        refreshPlotButton2.visible = False
        refreshPlotButton3.visible = False
        
        p.visible = False
        button2.visible = True
        button3.visible = False
        button4.visible = False
        whereisDatabutton.visible = True
        p0.visible = True
        data_table_from_rawlog.visible = True
    
visualize_mode_radio_button.on_change('active', visualize_mode_onchange)


ninix_id_textbox.visible = False
last_records_textbox.visible = False
button1.visible = False

select_parameter1.visible = False
select_parameter2.visible = False
select_parameter3.visible = False
parameter1_bias.visible = False
parameter2_bias.visible = False
parameter3_bias.visible = False
parameter1_scale.visible = False
parameter2_scale.visible = False
parameter3_scale.visible = False
refreshPlotButton1.visible = False
refreshPlotButton2.visible = False
refreshPlotButton3.visible = False

p.visible = False
button2.visible = False
whereisDatabutton.visible = False
p0.visible = False
data_table_from_rawlog.visible = False

layout1 = layout([
    [visualize_mode_radio_button],
    [WidgetBox(ninix_id_textbox)],
    [whereisDatabutton],
    [p0],
    [from_date, from_time, to_date, to_time],
    [last_records_textbox],
    [WidgetBox(button1)],
    [WidgetBox(button3), WidgetBox(button4)],
    [data_table_from_rawlog],
    [[select_parameter1, parameter1_bias, parameter1_scale, refreshPlotButton1], [select_parameter2, parameter2_bias, parameter2_scale, refreshPlotButton2], [select_parameter3, parameter3_bias, parameter3_scale, refreshPlotButton3]],
    [p],
    [WidgetBox(button2)]
#    [Spacer(height=50)],
#    row([column([label_text_input,label_button]), Spacer(width=100), data_table], background='beige')
])
layout1 = row(layout1, Spacer(width=100), data_table)
doc = curdoc()
doc.add_root(layout1)
doc.title = "NINIX Dashboard"






